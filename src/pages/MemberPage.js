import React from "react"

import "react-bulma-components/dist/react-bulma-components.min.css";

import MemberAdd from "./../components/MemberAdd"
import MemberList from "./../components/MemberList"

import {
	Section,
	Card,
	Columns,
	Button 

} from "react-bulma-components"

class MemberPage extends React.Component {
	state = {
		message: "testing one on one", 
		status: "pending"
	}

	render(){

			const sectionStyle = {
			paddingTop: "15px", 
			paddingBottom: "15px",	
			backgroundColor:"lightyellow",

			}
		
		return (

		 <React.Fragment> 

		 <Section size="small">
		 <Columns> 
		 <Columns.Column size={3} style={sectionStyle}>
		 	
		 <MemberAdd/> 

		 </Columns.Column>
		 <Columns.Column size={9}> 
		  <MemberList message={this.state.message} stat={this.state.status}/> 
		 		
		</Columns.Column> 
		 </Columns> 
		 </Section>

		</React.Fragment> 

		)
	}
}




export default MemberPage

//follows the name of the file taht we use

