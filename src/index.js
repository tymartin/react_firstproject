import React from 'react';
import ReactDOM from 'react-dom';

//import bulma components

// import "react-bulma-components/dist/react-bulma-components.min.css";

// import { 
// 	Navbar,
// 	Heading,
// 	Section, 
// 	Columns,
// 	Card
// } from "react-bulma-components"

//IMPORT MEMBER PAGE
import MemberPage from "./pages/MemberPage"
import AppNavbar from "./components/AppNavbar"


//setup the root div

const divRoot = document.getElementById('root'); 

//Internal Styling

// let sectionStyle = {
// 	paddingTop: "15px",
// 	paddingBottom: "15px",
// 	color:"white",
// 	backgroundColor:"gray"
// }




//setup the Navbar 1
const pageComponent = 
	<React.Fragment> 
	<AppNavbar/>
	<MemberPage/>
	</React.Fragment> 
//Render the Navbar

ReactDOM.render(pageComponent, divRoot)