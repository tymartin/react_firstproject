import React from "react"; 

import "react-bulma-components/dist/react-bulma-components.min.css";

import {Navbar}  from "react-bulma-components"; 

class AppNavbar extends React.Component {

	render() {
		return(
			<Navbar className="is-black"> 
			 	<Navbar.Brand> 
			 		<Navbar.Item> 
			 			<strong> Survibes Ako </strong> 
			 		</Navbar.Item>
			 		<Navbar.Burger/>
			 	</Navbar.Brand>
			 	<Navbar.Menu>
			 			<Navbar.Container>
			 				<Navbar.Item> Member</Navbar.Item> 
			 				<Navbar.Item> Teams</Navbar.Item> 
			 				<Navbar.Item> Tasks</Navbar.Item> 
			 			</Navbar.Container> 
			 	</Navbar.Menu> 
 			</Navbar>
			);
	}


}

export default AppNavbar