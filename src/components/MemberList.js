import React from "react"; 

import "react-bulma-components/dist/react-bulma-components.min.css";


import {
	Heading, 
	Card,
	Columns,
	Button 
} from "react-bulma-components"

class MemberList extends React.Component {
	state = {
		members : []
	}

	componentDidMount() {
		let retrievedMembers = [
		{
			firstname: 'Jun',
			lastname: 'Hunyo',
			position: 'Instructor',
			team: 'Instructor Team'
		}, 
		{
			firstname: 'Juliana',
			lastname: 'Policarpio',
			position: 'Lead Design',
			team: '	Swift Team'
		}, 
		{
			firstname: 'Antonio',
			lastname: 'Panopio',
			position: 'Developer',
			team: 'Tech Team'
		}
		]; 

		this.setState({members : retrievedMembers})
	}

	componentDidUpdate() {
		console.log(this.state.members); 
	}

	render() {
		const message = this.props.message;
		const status = this.props.stat; 
		return(
	
	 			<Card> 
	 				<Card.Header> 
	 					<Card.Header.Title> Member List
	 					</Card.Header.Title> 
	 				</Card.Header> 

	 				<Card.Content> 
	 					<div className="table-container"> 
	 						<table className="table is-fullwidth is-bordered"> 
	 							<thead> 
	 								<th>Member Name </th> 
	 								<th>Position</th> 
	 								<th>Team </th> 
	 								<th>Action </th> 
	 							</thead> 
	 							<tbody>
	 								{
	 									this.state.members.map(member =>{ 
	 									return (
			 							<tr> 
			 								<td> { member.firstname + ' ' + member.lastname }</td>
			 								<td> { member.position }</td>
			 								<td> { member.team }</td>
			 								<td> { status + ' ' + message }</td>
			 							</tr> 
	 									)
	 								}) 
	 								}
	 							</tbody>  
	 						</table> 
	 					</div> 
	 				</Card.Content> 
	 			</Card> 
	 	
			);
	}


}

export default MemberList; 