import React from "react"; 

import "react-bulma-components/dist/react-bulma-components.min.css";


import {
	Heading, 
	Card,
	Columns,
	Button 
} from "react-bulma-components"

class MemberAdd extends React.Component {

	render() {
	

		return(
		<React.Fragment> 
			<Card> 
				<Card.Header>	
					<Card.Header.Title>Add Member Here:</Card.Header.Title>
				</Card.Header>
	 			<form> 
	 			<Card.Content> 
	 				<div className="content"> 
	 					<label htmlfor="first-name"> First Name: </label>
	 					<input className="input is-primary" name="first-name" id="first-name"></input> 
	 					<label htmlfor="last-name"> last Name: </label>
	 					<input className="input is-primary" name="last-name" id="last-name"></input> 
	 					<label htmlfor="position-name"> Position Name: </label>
	 					<input className="input is-primary" name="position-name" id="position-name"></input> 
	 				</div> 

	 				<label htmlfor="Teams"> Teams: </label>
	 				<div class="select"> 
	 				<select> 
	 					
	 					<option>Red Team </option> 
	 					<option>Blue Team </option> 
	 					<option>Yellow Team </option> 
	 				</select> 	
	 				</div>
	 				
	 			</Card.Content> 

	 			<div className="panel-block">
	 				<Button className="Button is-success"> 
	 					Add Member 
	 				</Button>
	 			</div>  
	 			</form>
	 		 </Card>  	
	 		</React.Fragment>
			);
	}


}

export default MemberAdd